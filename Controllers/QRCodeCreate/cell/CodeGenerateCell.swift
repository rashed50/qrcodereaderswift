//
//  CodeGenerateCell.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class CodeGenerateCell: UITableViewCell {
    
    @IBOutlet weak var CodeNameLabel: UILabel!
    @IBOutlet weak var CodeNameText: UITextField!
    @IBOutlet weak var CodeDetailsLabel: UILabel!
    @IBOutlet weak var addButtonWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var numberTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
          return UINib(nibName: identifier, bundle: nil)
      }
      static var identifier: String {
          return String(describing: CodeGenerateCell.self)
      }
    
}
