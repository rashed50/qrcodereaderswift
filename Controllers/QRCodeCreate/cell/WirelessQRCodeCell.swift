//
//  WirelessQRCodeCell.swift
//  QRCodeReader.swift
//
//  Created by Rashedul Hoque on 15/8/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class WirelessQRCodeCell: UITableViewCell {

    
    @IBOutlet weak var ssidTextField: UITextField!
    @IBOutlet weak var encryptionButton: UIButton!
    @IBOutlet weak var encryptionLabel: UILabel!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib {
            return UINib(nibName: identifier, bundle: nil)
        }
        static var identifier: String {
            return String(describing: WirelessQRCodeCell.self)
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
