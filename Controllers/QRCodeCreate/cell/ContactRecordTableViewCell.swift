//
//  ContactRecordTableViewCell.swift
//  QRCodeReader.swift
//
//  Created by Rashedul Hoque on 16/8/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class ContactRecordTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var nameTextFied: UITextField!
     @IBOutlet weak var homePhoneTextFied: UITextField!
     @IBOutlet weak var companyTextFied: UITextField!
     @IBOutlet weak var titleTextFied: UITextField!
     @IBOutlet weak var phoneTextFied: UITextField!
     @IBOutlet weak var emailTextFied: UITextField!
     @IBOutlet weak var addressTextFied: UITextField!
     @IBOutlet weak var websiteTextFied: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib {
            return UINib(nibName: identifier, bundle: nil)
        }
        static var identifier: String {
            return String(describing: ContactRecordTableViewCell.self)
        }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
