//
//  createHistoryTableCell.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class createHistoryTableCell: UITableViewCell {

    @IBOutlet weak var createCodeImage: UIImageView!
    @IBOutlet weak var createCodeNameLabel: UILabel!
    @IBOutlet weak var createCodeDateLabel: UILabel!
    @IBOutlet weak var favouriteImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
        
    }
    static var nib: UINib {
             return UINib(nibName: identifier, bundle: nil)
         }
         static var identifier: String {
             return String(describing: createHistoryTableCell.self)
         }
        
}
