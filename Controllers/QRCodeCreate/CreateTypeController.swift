//
//  CreateTypeController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 25/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class CreateTypeController: UIViewController {

   var createTypeList = ["URL","Phone Number","Text","Email Address","Contact Record","SMS","Wireless Network"] //,"Calender Event"
        override func viewDidLoad() {
            super.viewDidLoad()
         
        }
        
 
   override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        setNavigationBarBackButton()
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Choose QR Code Type")
        self.title = "Choose QR Code Type"
      Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
          }
          
          
          func setNavigationBarBackButton() {

                      self.navigationItem.setHidesBackButton(true, animated:false)
                      let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                      let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
                      if let imgBackArrow = UIImage(named: "back-arrow-png") {
                          imageView.image = imgBackArrow
                      }
                      view.addSubview(imageView)
                      let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
                      view.addGestureRecognizer(backTap)
                      let leftBarButtonItem = UIBarButtonItem(customView: view)
                      self.navigationItem.leftBarButtonItem = leftBarButtonItem
                  }

                  @objc func backToMainViewController() {
                             let transition = CATransition()
                             transition.duration = 0.4
                             transition.type = CATransitionType.push
                             transition.subtype = CATransitionSubtype.fromLeft
                             self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                             self.navigationController?.popViewController(animated: false)
                       
                   }

    }
    extension CreateTypeController: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return createTypeList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "createCell", for: indexPath)
            cell.textLabel?.text = createTypeList[indexPath.row]
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
           // let index = indexPath.row
            
            if #available(iOS 13.0, *) {
                if let vc = storyboard?.instantiateViewController(identifier: "qrCodeGenerateController") as? QRCodeGenerateController{
                    vc.codeType = createTypeList[indexPath.row]
                    vc.codePlaceholder = Helper.GetPlacheholderText(index: Int8(indexPath.row))
                    vc.codeDetails = Helper.GetDetailsText(index: Int8(indexPath.row))
                    navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                if let vc = storyboard?.instantiateViewController(withIdentifier:"qrCodeGenerateController") as? QRCodeGenerateController{
                                   vc.codeType = createTypeList[indexPath.row]
                                   vc.codePlaceholder = Helper.GetPlacheholderText(index: Int8(indexPath.row))
                                   // vc.OutputLabel.text = imagesLabel[jontroIndex]
                                   navigationController?.pushViewController(vc, animated: true)
                               }
            }
        }
        
        
    }



