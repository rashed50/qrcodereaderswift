//
//  QRCodeGenerateController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 25/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import LocalAuthentication

class CodeCreateHistory: UIViewController {
    
    
    var screenLock = UIView()
    var generateHistory   = [CodeInfo]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(UserDefaults.standard.getAppLock(forKey: .appLock)){
                   showHideAppLockScreenUI(forHide: false)
                   authenticationWithTouchID()
               }
        self.tableView.register(createHistoryTableCell.nib, forCellReuseIdentifier: createHistoryTableCell.identifier)
      Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Generated QR Code")
       
        generateHistory = DBHelper.getAllCodeInformationWithOperationType(optType: 1)  // 1 for create qrcode
        self.tableView.reloadData()
    } 
       
       override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
        print(self.tabBarController?.tabBar.frame.size.height)
        
        let bottomMargin = (self.tabBarController?.tabBar.frame.size.height)!
         //   UIScreen.main.bounds.height
       // self.view.layoutMargins = UIEdgeInsetsMake(0, 0, bottomMargin, 0);
        
       }
   
    
}
extension CodeCreateHistory: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return generateHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "createHistoryTableCell", for: indexPath) as! createHistoryTableCell
        let codeInfo : CodeInfo = generateHistory[indexPath.row]
        cell.createCodeNameLabel.text = codeInfo.codeName
        
        
        let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage)
        cell.createCodeImage?.image = UIImage.init(contentsOfFile: imagePath)
        cell.createCodeDateLabel.text = Helper.getStringFromDate(date:codeInfo.createDate)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(codeActionSheet))
        cell.createCodeImage?.addGestureRecognizer(tapGestureRecognizer)
        cell.favouriteImageView.isHidden = true
        if codeInfo.isFavorite{
            cell.favouriteImageView.image = Helper.getFavouriteImage()
            cell.favouriteImageView.isHidden = false
            
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        codeActionSheet(index: indexPath.row)
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    @objc func codeActionSheet(index:Int)
    {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let information = UIAlertAction(title: "Preview", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if #available(iOS 13.0, *) {
                if let vc = self.storyboard?.instantiateViewController(identifier: "qRCodePreviewController") as? QRCodePreviewController{
                    vc.codeInfo = self.generateHistory[index]
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                
            }
            
            
        })
        
        let codeInfo = self.generateHistory[index]
        let favouriteActionText = codeInfo.isFavorite ? "Unfavourite" : "Favourite"
        let favouriteAction = UIAlertAction(title: favouriteActionText, style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            codeInfo.isFavorite = !codeInfo.isFavorite
            self.generateHistory[index] = codeInfo
            DBHelper.updateCodeInfoAsFavouriteUnFavourite(isFavourite: codeInfo.isFavorite, id: codeInfo.id)
            self.tableView.reloadData()
            
        })
        let shareAction = UIAlertAction(title: "Share", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.shareButtonPressed(codeInfo: codeInfo)
        })
        let deleteAction = UIAlertAction(title: "Delete", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            DBHelper.deleteCodeInfo(id: codeInfo.id)
            self.generateHistory.remove(at: index)
            self.tableView.reloadData()
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(information)
        optionMenu.addAction(favouriteAction)
        optionMenu.addAction(shareAction)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
     
  func shareButtonPressed(codeInfo : CodeInfo){
         
              
             let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage)
             let image = UIImage(contentsOfFile: imagePath)
              
                //if image != nil
               // {
                   //let imageToShare = [ image! ]
                    let objectsToShare = [image!] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                    self.present(activityVC, animated: true, completion: nil)
             //   }

        }
         
    func showHideAppLockScreenUI(forHide bool :Bool) {
        
        
        let window = UIApplication.shared.keyWindow!


        
        if(bool){
            
            screenLock.removeFromSuperview()
        }else {
            screenLock = UIView.init(frame: window.bounds)
            screenLock.backgroundColor = UIColor.black
            window.addSubview(screenLock)
        }
    }
    
    func authenticationWithTouchID() {
              
              let localAuthenticationContext = LAContext()
              localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"

              var authorizationError: NSError?
              let reason = "Authentication required to access the secure data"

              if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
                  
                  localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
                      
                      if success {
                          DispatchQueue.main.async() {
                              
                            self.showHideAppLockScreenUI(forHide: true)
                            //  self.present(alert, animated: true, completion: nil)
                          }
                          
                      } else {
                          // Failed to authenticate
                          guard let error = evaluateError else {
                              return
                          }
                          print(error)
                      
                      }
                  }
              } else {
                  
                  guard let error = authorizationError else {
                      return
                  }
                  print(error)
              }
          }
    
}





