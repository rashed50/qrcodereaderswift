//
//  QRCodeGenerateController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 25/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import PhoneNumberKit

class QRCodeGenerateController: UIViewController {
    
    var codeType:String?
    var codePlaceholder:String?
    var codeDetails:String?
    
    // Wireless Network Code
    var encryptionDefaultText = "WEP2"
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var qrCodeCreateButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //["URL","Phone Number","Text","Email Address","Contact Record","SMS","Calender Event","Wireless Network"]
        if codeType == "Phone Number" || codeType == "URL" || codeType == "Text" || codeType == "Email Address" || codeType == "SMS"{
            self.tableView.register(CodeGenerateCell.nib, forCellReuseIdentifier: CodeGenerateCell.identifier)
        }else if codeType == "Contact Record" {
            
            self.tableView.register(ContactRecordTableViewCell.nib, forCellReuseIdentifier: ContactRecordTableViewCell.identifier)
        }
        else {
            self.tableView.register(WirelessQRCodeCell.nib, forCellReuseIdentifier: WirelessQRCodeCell.identifier)
        }
        setNavigationBarBackButton()
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Generate QR Code")
        self.title = "Generate QR Code"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // qrCodeCreateButton.tintColor = Helper.getTextColor()
        Helper.SetBarButtonUIProperty(button: qrCodeCreateButton, fontSize: 18)
        //  qrCodeCreateButton.setTitleColor(getTextColor(), for: .normal)
        //   qrCodeCreateButton.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
        
        Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
    }
    
    
    func setNavigationBarBackButton() {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        if let imgBackArrow = UIImage(named: "back-arrow-png") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
        view.addGestureRecognizer(backTap)
        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backToMainViewController() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func showErrorMessageAlert(title:String,mgs:String) {
         
        let alert =  UIAlertController.init(title: title, message: mgs, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func CodeGenerateActionButton(_ sender: Any) {
        
        
        
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        
        
        var codeInfoText = ""
        
        if codeType == "Phone Number"{
            let cell = self.tableView.cellForRow(at: indexPath) as! CodeGenerateCell
                     codeInfoText = cell.CodeNameText.text ?? ""
                     
                     
                     do {
               
                         let phoneNumber = try PhoneNumberKit().parse(codeInfoText)
                         codeInfoText = phoneNumber.numberString
                     }
                     catch {
                         print("Generic parser error")
                        showErrorMessageAlert(title: "Error", mgs: "Pleae Enter Valid Phone Number")
                        return
                     }
        }
        
        else if codeType == "URL" {
            let cell = self.tableView.cellForRow(at: indexPath) as! CodeGenerateCell
                codeInfoText = cell.CodeNameText.text ?? ""
            if !String.isValidURL(codeInfoText)() {
                showErrorMessageAlert(title: "Error", mgs: "Pleae Enter Valid URL")
                return
            }
            
        }else if codeType == "Email Address" {
            let cell = self.tableView.cellForRow(at: indexPath) as! CodeGenerateCell
                codeInfoText = cell.CodeNameText.text ?? ""
            if !String.isValidEmail(codeInfoText)() {
                showErrorMessageAlert(title: "Error", mgs: "Pleae Enter Valid Email Address")
                return
            }
            
        }
        else if  codeType == "Text" || codeType == "SMS"{
            let cell = self.tableView.cellForRow(at: indexPath) as! CodeGenerateCell
            codeInfoText = cell.CodeNameText.text ?? ""
            
            
            
        }else if codeType == "Wireless Network"{
            let cell = self.tableView.cellForRow(at: indexPath) as! WirelessQRCodeCell
            codeInfoText = String(format: "Wifi: %@,T: %@,P: %@", cell.ssidTextField.text ?? "",cell.encryptionLabel.text ?? "",cell.passwordTextField.text ?? "")
            
        }else if codeType == "Contact Record"{
            let cell = self.tableView.cellForRow(at: indexPath) as! ContactRecordTableViewCell
            codeInfoText = String(format: "Name:%@,H.Phone:%@,Company:%@,Title:%@,Phone:%@,Email:%@,Address:%@,Website:%@", cell.nameTextFied.text ?? "",cell.homePhoneTextFied.text ?? "",cell.companyTextFied.text ?? "",cell.titleTextFied.text ?? "",cell.phoneTextFied.text ?? "",cell.emailTextFied.text ?? "",cell.addressTextFied.text ?? "",cell.websiteTextFied.text ?? "")
        }
        
        if codeInfoText == ""{
            codeInfoText = codeType!
        }
        
        var qrImage :CIImage!
        let swiftLeeOrangeColor = UserDefaults.standard.getQRCodeColor(forKey: .qrcodeColor)
        if UserDefaults.standard.getIsAddCustomLogoWithQRCode(forKey: .customLogoWithQRCode){
            let swiftLeeLogo = UIImage.init(contentsOfFile: Helper.getPathForQrCodeLogo())
            qrImage = String(codeInfoText).qrImage(using: swiftLeeOrangeColor, logo: swiftLeeLogo)
        }else {
            qrImage = String(codeInfoText).qrImage(using: swiftLeeOrangeColor)
        }
        if(qrImage != nil){
            let qrCodeImage  =  UIImage(ciImage: qrImage!)
            
            let filename = String(format: "%ld.jpg",Date().millisecondsSince1970)
            
            saveQRCodeImageToPath(fileName: filename, qrImg: qrCodeImage)
            let codeInfo = CodeInfo.init(id: 0, codeName:  codeInfoText, codeType: 1, date: Date.init(), operationType: 1, codeImage:filename , isFavorite: false)
            DBHelper.inseCodeInformation(codeInfo: codeInfo)
            AppStoreReviewManager.requestReviewIfAppropriate()
            self.navigationController?.popToRootViewController(animated: true)
            
        }
    }
    
    
    
    func saveQRCodeImageToPath(fileName: String, qrImg:UIImage) {
        let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),fileName)
        print(imagePath)
        let data = qrImg.jpegData(compressionQuality: 1.0)//Set image quality here
        
        let fileURL = URL.init(fileURLWithPath: imagePath)
        do {
            try data?.write(to: fileURL, options: .atomic)
        } catch {
            print("error:", error)
        }
    }
    
}

extension QRCodeGenerateController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if codeType == "Phone Number" || codeType == "URL" || codeType == "Text" || codeType == "Email Address" || codeType == "SMS"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CodeGenerateCell", for: indexPath) as! CodeGenerateCell
            cell.CodeNameLabel.text = codeType
            cell.CodeNameText.placeholder = codePlaceholder
            cell.CodeNameText.becomeFirstResponder()
            cell.CodeNameText.keyboardType = .default
            cell.CodeDetailsLabel.text = codeDetails
            cell.CodeNameText.delegate = self as UITextFieldDelegate
            
            if codeType == "Phone Number" {
                cell.addButtonWidthConstant.constant = 34.0
                cell.CodeNameText.keyboardType = .numberPad
            }else {
                cell.addButtonWidthConstant.constant = 0.0
            }
            if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
                cell.CodeNameText.isEnabled = false
            }
            return cell
            
        }else if codeType == "Contact Record"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactRecordTableViewCell", for: indexPath) as! ContactRecordTableViewCell
            
            cell.nameTextFied.placeholder = codeType
            cell.nameTextFied.becomeFirstResponder()
            
            cell.nameTextFied.delegate = self
            cell.homePhoneTextFied.placeholder = "Phone Number"
            cell.companyTextFied.placeholder = "Company Name"
            cell.titleTextFied.placeholder = "Title"
            cell.emailTextFied.placeholder = "Email"
            cell.addressTextFied.placeholder = "Address"
            cell.websiteTextFied.placeholder = "Website"
            cell.phoneTextFied.placeholder = "Company Phone"
            
            if UserDefaults.standard.string(forKey: .inAppPurchase) !=  nil{
                cell.nameTextFied.isEnabled = false
                cell.homePhoneTextFied.isEnabled = false
                cell.companyTextFied.isEnabled = false
                cell.titleTextFied.isEnabled = false
                cell.phoneTextFied.isEnabled = false
                cell.emailTextFied.isEnabled = false
                cell.addressTextFied.isEnabled = false
                cell.websiteTextFied.isEnabled = false
            }
            
            return cell
        }
        else{
            
            let  cell = tableView.dequeueReusableCell(withIdentifier: "WirelessQRCodeCell", for: indexPath) as! WirelessQRCodeCell
            cell.ssidTextField.placeholder = codeType
            cell.ssidTextField.becomeFirstResponder()
            cell.ssidTextField.delegate = self as UITextFieldDelegate
            cell.encryptionLabel.text = encryptionDefaultText
            cell.passwordTextField.placeholder = codeType
            cell.encryptionButton.addTarget(self, action: #selector(encryptionButtonAction), for: .touchUpInside)
            
            if UserDefaults.standard.string(forKey: .inAppPurchase) !=  nil{
                cell.ssidTextField.isEnabled = false
                cell.ssidTextField.isEnabled = false
                cell.encryptionButton.isEnabled = false
                cell.passwordTextField.isEnabled = false
            }
            
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    @objc func encryptionButtonAction(_ button: UIButton) {
        
    }
   
  
    
    
}



extension QRCodeGenerateController : UITextFieldDelegate{
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    
    
}
