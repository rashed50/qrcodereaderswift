//
//  ColorPickerViewController.swift
//  QRCodeReader.swift
//
//  Created by Rashedul Hoque on 11/8/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import CULColorPicker

class ColorPickerViewController: UIViewController,ColorPickerViewDelegate {
    
    var selectedColor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()

        colorPicker.delegate = self
        selectedColor = UserDefaults.standard.getQRCodeColor(forKey: .qrcodeColor)
        previewSelectColorView.backgroundColor = selectedColor
        brightnessSlider.value = Float(selectedColor.rgb.alpha)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
           Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Choose QR Code Color")
       setNavigationBarBackButton()
             }
             
             
             func setNavigationBarBackButton() {

                         self.navigationItem.setHidesBackButton(true, animated:false)
                         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                         let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
                         if let imgBackArrow = UIImage(named: "back-arrow-png") {
                             imageView.image = imgBackArrow
                         }
                         view.addSubview(imageView)
                         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
                         view.addGestureRecognizer(backTap)
                         let leftBarButtonItem = UIBarButtonItem(customView: view)
                         self.navigationItem.leftBarButtonItem = leftBarButtonItem
                     }

                     @objc func backToMainViewController() {
                                let transition = CATransition()
                                transition.duration = 0.4
                                transition.type = CATransitionType.push
                                transition.subtype = CATransitionSubtype.fromLeft
                                self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                                self.navigationController?.popViewController(animated: false)
                          
                      }
    
    
    
    func colorPickerWillBeginDragging(_ colorPicker: ColorPickerView) {
        
    }
    
    func colorPickerDidEndDagging(_ colorPicker: ColorPickerView) {
        
    }
    func colorPickerDidSelectColor(_ colorPicker: ColorPickerView) {
   
    //    previewSelectColorView.backgroundColor = colorPicker.selectedColor
        selectedColor = colorPicker.selectedColor
        previewAndSaveSelectedColor()
    }
    
    
    
    
    @IBOutlet weak var colorPicker: ColorPickerView!
    
    @IBOutlet weak var previewSelectColorView: UIView!
    
    @IBOutlet weak var brightnessSlider: UISlider!
    
    @IBAction func saveButtonAction(_ sender: UIBarButtonItem) {
        
       // self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func brightnessSlider(_ sender: UISlider) {
        
        previewAndSaveSelectedColor()
        
    }
   
    func previewAndSaveSelectedColor() {
         
        selectedColor = UIColor.init(red: selectedColor.rgb.red, green: selectedColor.rgb.green, blue: selectedColor.rgb.blue, alpha: CGFloat(brightnessSlider.value))
        previewSelectColorView.backgroundColor = UIColor.init(red: selectedColor.rgb.red, green: selectedColor.rgb.green, blue: selectedColor.rgb.blue, alpha: CGFloat(brightnessSlider.value))
         UserDefaults.standard.setQRCodeColor(hexaString:Helper.hexStringFromColor(color:selectedColor), forKey: .qrcodeColor)
    }
    
    
    
}
