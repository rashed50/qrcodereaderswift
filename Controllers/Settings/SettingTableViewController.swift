//
//  SettingTableViewController.swift
//  QRCodeReader.swift
//
//  Created by Rashedul Hoque on 9/8/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import MessageUI
import StoreKit
import SVProgressHUD

class SettingTableViewController: UITableViewController {
    
    //var inAppPurchseView =  UIView.init()
    //  var inAppPurchseView = InAppPurchaseView().loadNib() as! InAppPurchaseView // InAppPurchseView().loadNib()
    //   var inAppPurchaseProducts: [SKProduct] = []
    let userDefault = UserDefaults.standard
    
    @IBOutlet weak var appLockSwitchButton: UISwitch!
    @IBOutlet weak var saveScanDataSwitch: UISwitch!
    @IBOutlet weak var customLogoWithQRCodeSwitch: UISwitch!
    @IBOutlet weak var onlineSearchSwitch: UISwitch!
    @IBOutlet weak var openURLSwitch: UISwitch!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var uoloadLogoButton: UIButton!
    @IBOutlet weak var colorPickerButton: UIButton!

    
    let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        
        return formatter
    }()
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        
               NotificationCenter.default.addObserver(
               self,
               selector: #selector(inAppPurchaseDone),
               name: NSNotification.Name(rawValue: "IAPHelperPurchaseNotification"),
               object: nil)
               
           }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "App Setting")
        appLockSwitchButton.isOn = userDefault.getAppLock(forKey: .appLock)
        colorPickerButton.backgroundColor = userDefault.getQRCodeColor(forKey: .qrcodeColor)
        saveScanDataSwitch.isOn =   userDefault.getIsSaveScanData(forKey: .saveScanData)
        onlineSearchSwitch.isOn = userDefault.getIsOnlineSearchScanData(forKey: .onlineSearchAfterScan)
        customLogoWithQRCodeSwitch.isOn = userDefault.getIsAddCustomLogoWithQRCode(forKey: .customLogoWithQRCode)
        openURLSwitch.isOn = userDefault.getIsOpenURLInBrowser(forKey: .openURLInBrowser)
        colorPickerButton.layer.cornerRadius = colorPickerButton.frame.size.height / 2
        uoloadLogoButton.layer.cornerRadius = 10;
        
        if userDefault.getIsAddCustomLogoWithQRCode(forKey: .customLogoWithQRCode){
            hideLogoImgViewAndUploadButton(bol: false)
            let logoImg = UIImage.init(contentsOfFile: Helper.getPathForQrCodeLogo())
            if logoImg != nil {
                logoImageView.image = logoImg
                uoloadLogoButton.setTitle("Edit", for: .normal)
            }
        }else {
            hideLogoImgViewAndUploadButton(bol: true)
        }
      Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 6
        }
        else if section == 1{
            return 6
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 2  && userDefault.getIsAddCustomLogoWithQRCode(forKey: .customLogoWithQRCode) {
            hideLogoImgViewAndUploadButton(bol: false)
            return 120
        }
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 0 {
                shareThisApp()
            }
            else if indexPath.row == 1 {
                writeReview()
            }
            else if indexPath.row == 2  {
                // developer app
                openMyDeveloperApp()
            }
            else if indexPath.row == 3 {
                sendEmail()
            }
            else if indexPath.row == 4 {
                // frequently asked qs
            }
            else if indexPath.row == 5 {
                // about us
            }
        }
    }
    
    
    
    // MARK UI BUTTON ACTIONS
    
    
    
    
    @IBAction func AppLockSwithButtonAction(_ sender: UISwitch) {
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            userDefault.setAppLock(sender.isOn, forKey: .appLock)
        }
        
    }
    
    
    @IBAction func SaveScanData(_ sender: UISwitch) {
        
        
        
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            
            userDefault.setIsSaveScanData(sender.isOn, forKey: .saveScanData)
            if userDefault.getIsSaveScanData(forKey: .saveScanData) {
                userDefault.setIsSaveScanData(sender.isOn, forKey: .saveScanData)
            }
        }
    }
    
    
    @IBAction func AddCustomLogoWithQRCodeAction(_ sender: UISwitch) {
        
        
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            userDefault.setCustomLogoWithQRCode(sender.isOn, forKey: .customLogoWithQRCode)
            tableView.reloadData()
            hideLogoImgViewAndUploadButton(bol: !sender.isOn)
        }
    }
    
    @IBAction func colorPickerButtonAction(_ sender: UIButton) {
        
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            var colorPickVC = UIViewController()
            if #available(iOS 13.0, *) {
                colorPickVC = story.instantiateViewController(identifier: "ColorPickerViewController") as! ColorPickerViewController
            } else {
                colorPickVC = story.instantiateViewController(withIdentifier: "ColorPickerViewController") as! ColorPickerViewController
            }
            self.navigationController?.pushViewController(colorPickVC, animated: true)
            
        }
        
    }
    
    @IBAction func OnlineSearchButtonAction(_ sender: UISwitch) {
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            userDefault.setOnlineSearchScanData(sender.isOn, forKey: .onlineSearchAfterScan)
        }
    }
    
    @IBAction func OpenUrlInBrowserAction(_ sender: UISwitch) {
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
            checkInAppPurchase()
        }else{
            userDefault.setOpenURLInBrowser(sender.isOn, forKey: .openURLInBrowser)
        }
    }
    
    let imagePicker = UIImagePickerController()
    @IBAction func addLogoButtonAction(_ sender: UIButton) {
        
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    
    // MARK: - USER DEFINE METHODS
    
    @objc func inAppPurchaseDone(notification: NSNotification){
          print(notification.userInfo!)
          let obj = notification.userInfo as! [String:String]
          
          if obj["message"] == "error"{
              InAppPurchaseView.dismissProgressHud()
          }else {
              InAppPurchaseView.inAppPurchaseDone()
          }
              
      }
    func hideLogoImgViewAndUploadButton(bol:Bool) {
        uoloadLogoButton.isHidden = bol
        logoImageView.isHidden = bol
    }
    
    private let productURL = URL(string: "https://apps.apple.com/us/app/id1067631975")!
    
    private func writeReview() {
        var components = URLComponents(url: productURL, resolvingAgainstBaseURL: false)
        components?.queryItems = [
            URLQueryItem(name: "action", value: "write-review")
        ]
        
        guard let writeReviewURL = components?.url else {
            return
        }
        
        UIApplication.shared.open(writeReviewURL)
    }
    
    private func shareThisApp() {
        let activityViewController = UIActivityViewController(activityItems: [productURL],
                                                              applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    func openMyDeveloperApp() {
        let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
        var viewCon : UIViewController
        if #available(iOS 13.0, *) {
            viewCon = story.instantiateViewController(identifier: "pageContainerViewController")
        } else {
            viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
        }
        
        self.navigationController?.pushViewController(viewCon, animated: true)
    }
    
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as MFMailComposeViewControllerDelegate
            
            mail.setToRecipients(["asma0840500@gmail.com"])
            mail.title = "User Feedback and Suggestion"
            mail.setMessageBody("<p>Your Feedback and Suggestions is Our Motivation</p>", isHTML: true)
            
            self.present(mail, animated: true, completion: {
            })
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        //resizing the rect using the ImageContext
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    // MARK IN APP PURCHSE CHEKING
    
    func checkInAppPurchase() {
        
        
        showHideAppLockScreenUI(forHide: false)
    }
    
    
    func showHideAppLockScreenUI(forHide bool :Bool) {
        
        let window = UIApplication.shared.keyWindow!
        if(bool){
            
        }else {
            
            let appDele = UIApplication.shared.delegate as! AppDelegate
            if appDele.inAppPurchaseProducts.count > 0{
                let product = appDele.inAppPurchaseProducts[0]
                window.addSubview(InAppPurchaseView.setViewInformation(productTitle: product.localizedDescription, price: priceFormatter.string(from: product.price)!, frame:window.frame))
               
            }else {
                window.addSubview(InAppPurchaseView.setViewInformation(productTitle: "product.localizedDescription", price:"0", frame:window.frame))
            }
        }
    }
    
    
    
}

extension SettingTableViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension SettingTableViewController: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        var im = info[.originalImage] as? UIImage
        logoImageView.image = im
        var path =  Helper.getQRCodeImagesDirectory()
        path = String(format: "%@/%@", path,"logo.jpg")
        
        im = resizeImage(image: im!, targetSize: CGSize.init(width: 40, height: 40))
        
        let imageData = im!.jpegData(compressionQuality: 1.0)//Set image quality here
        
        
        
        let fileURL = URL.init(fileURLWithPath: path)
        do {
            try imageData?.write(to: fileURL, options: .atomic)
        } catch {
            print("error:", error)
        }
        
        dismiss(animated: true, completion: nil)
        
    }
}



extension SettingTableViewController: UINavigationControllerDelegate {
    
}
