//
//  QRCodePreviewController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class QRCodePreviewController: UIViewController {

    var codeInfo = CodeInfo.init()
    
    @IBOutlet weak var qrcodeImageView: UIImageView!
    
    @IBOutlet weak var codeInfoLabel: UILabel!
    
    
    @IBOutlet weak var reateDateLabel: UILabel!
    
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "QRCode Preview")
        qrcodeImageView.image = UIImage.init(contentsOfFile: String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage))
        codeInfoLabel.text = codeInfo.codeName
        reateDateLabel.text = Helper.getStringFromDate(date: codeInfo.createDate)
        setNavigationBarBackButton()
        Helper.SetBarButtonUIProperty(button: shareButton, fontSize: 18)
        Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
    }
    
    
    @IBAction func shareButtonAction(_ sender: Any) {
        
        shareButtonPressed(codeInfo: codeInfo)
    }
    func shareButtonPressed(codeInfo : CodeInfo){
            
                 
                let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage)
                let image = UIImage(contentsOfFile: imagePath)
                 
                   //if image != nil
                  // {
                      //let imageToShare = [ image! ]
                       let objectsToShare = [image!] as [Any]
                       let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                       activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                       self.present(activityVC, animated: true, completion: nil)
                //   }

           }
    
    func setNavigationBarBackButton() {

                self.navigationItem.setHidesBackButton(true, animated:false)
                let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
                if let imgBackArrow = UIImage(named: "back-arrow-png") {
                    imageView.image = imgBackArrow
                }
                view.addSubview(imageView)
                let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
                view.addGestureRecognizer(backTap)
                let leftBarButtonItem = UIBarButtonItem(customView: view)
                self.navigationItem.leftBarButtonItem = leftBarButtonItem
            }

            @objc func backToMainViewController() {
                       let transition = CATransition()
                       transition.duration = 0.4
                       transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
                       self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                       self.navigationController?.popViewController(animated: false)
                 
             }

    

}
