/*
 * QRCodeReader.swift
 *
 * Copyright 2014-present Yannick Loriot.
 * http://yannickloriot.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

import UIKit
import LocalAuthentication
import StoreKit
 import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var inAppPurchaseProducts: [SKProduct] = []
    
    
    

      let bannerAddUitLiveID = "ca-app-pub-3940256099942544/2934735716"  //  Test ID
     //   let rewardAddUitLiveID = "" // Test ID
    //  let bannerAddUitLiveID = "ca-app-pub-7072588706544361/9413308325"          //  Live ID
    //  let rewardAddUitLiveID = ""          // Live ID
      var adMobBannerView: GADBannerView!
      var adMobBannerOnNavigationBar: GADBannerView!
      
    
    
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  
    DBHelper.CreateDatabase()
    if !UserDefaults.standard.bool(forKey: .firstTimeInitialize){
        
        UserDefaults.standard.set(false, forKey: .appLock)
        UserDefaults.standard.set(false, forKey: .openURLInBrowser)
        UserDefaults.standard.set(false, forKey: .onlineSearchAfterScan)
        UserDefaults.standard.set(false, forKey: .customLogoWithQRCode)
        UserDefaults.standard.set(true, forKey: .saveScanData)
        UserDefaults.standard.set(true, forKey: .firstTimeInitialize)
        Helper.createQRCodeImagesDirectory()
    }
    
    initializeForAdsShowing()
    checkInAppPurchase()
    return true
  }
  
    func checkInAppPurchase() {
        
        RazeFaceProducts.store.requestProducts{[weak self] sucess , products in
            if(sucess){
                print(products as Any)
                self!.inAppPurchaseProducts = products!
                
            }
        }
    }
    
    
    
    

 
    func initializeForAdsShowing() {
         
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["phone 11 pro"];
            initializeGoogleAdmob()
        
    }

       
       func initializeGoogleAdmob() {
               
              adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
              adMobBannerView.adUnitID = bannerAddUitLiveID
             
        adMobBannerView.delegate = self as? GADBannerViewDelegate
              adMobBannerView.load(GADRequest())
              //adMobBannerView.isHidden = true
            
          
       }
       
       func initializeGoogleAdmobOnNavigationBar() {
               
                adMobBannerOnNavigationBar = GADBannerView(adSize: kGADAdSizeBanner)
                adMobBannerOnNavigationBar.adUnitID = bannerAddUitLiveID
        adMobBannerOnNavigationBar.delegate = self as? GADBannerViewDelegate
                adMobBannerOnNavigationBar.load(GADRequest())
                
         }
       
       //
       // MARK GOOGLE ADMOB BANNER DELEGATE
       //
       
       /// Tells the delegate an ad request failed.
          func adView(_ bannerView: GADBannerView,
              didFailToReceiveAdWithError error: GADRequestError) {
            print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
          }

          /// Tells the delegate that a full-screen view will be presented in response
          /// to the user clicking on an ad.
          func adViewWillPresentScreen(_ bannerView: GADBannerView) {
            print("adViewWillPresentScreen")
          }

          /// Tells the delegate that the full-screen view will be dismissed.
          func adViewWillDismissScreen(_ bannerView: GADBannerView) {
            print("adViewWillDismissScreen")
          }

          /// Tells the delegate that the full-screen view has been dismissed.
          func adViewDidDismissScreen(_ bannerView: GADBannerView) {
            print("adViewDidDismissScreen")
          }

          /// Tells the delegate that a user click will open another app (such as
          /// the App Store), backgrounding the current app.
          func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
            print("adViewWillLeaveApplication")
          
          }
       
       //
       // MARK GOOGLE ADMOB BANNER DELEGATE
       //
       
    
        func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
            print("Reward received with currency: \(reward.type), amount \(reward.amount).")
          }
       
       func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
         print("Rewarded ad presented.")
       }

       func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
         print("Rewarded ad failed to present.")
       }
      
       
       func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
         print("Rewarded ad dismissed.")
       }
       

    
    
    
    
    
    
    
    
    
   
}

