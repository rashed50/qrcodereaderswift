//
//  WebViewController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 25/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController {
    var webView:WKWebView!
    var search:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Online Searching Result")
        if let url = URL(string: search) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
       Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
    }
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
            Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Online Searching Result")
       setNavigationBarBackButton()
             }
             
             
             func setNavigationBarBackButton() {

                         self.navigationItem.setHidesBackButton(true, animated:false)
                         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                         let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
                         if let imgBackArrow = UIImage(named: "back-arrow-png") {
                             imageView.image = imgBackArrow
                         }
                         view.addSubview(imageView)
                         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
                         view.addGestureRecognizer(backTap)
                         let leftBarButtonItem = UIBarButtonItem(customView: view)
                         self.navigationItem.leftBarButtonItem = leftBarButtonItem
                     }

                     @objc func backToMainViewController() {
                                let transition = CATransition()
                                transition.duration = 0.4
                                transition.type = CATransitionType.push
                                transition.subtype = CATransitionSubtype.fromLeft
                                self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                                self.navigationController?.popViewController(animated: false)
                          
                      }


}
