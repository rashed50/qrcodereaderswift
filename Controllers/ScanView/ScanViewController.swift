/*
 * QRCodeReader.swift
 *
 * Copyright 2014-present Yannick Loriot.
 * http://yannickloriot.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

import AVFoundation
import UIKit
import SafariServices
class ScanViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    
    
    
    @IBOutlet var buttonViewContainer: UIView!
    @IBOutlet weak var openGalleryButton: UIButton!
    
    @IBOutlet weak var openCameraButton: UIButton!
    var load = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        Helper.SetButtonUIProperty(button: openCameraButton, title: "", fontSize: 18)
        Helper.SetButtonUIProperty(button: openGalleryButton, title: "", fontSize: 18)
       // Helper.setNavigationBarProperty(navbar: self.navigationController?, size: 18, title: "Scan QR Code")
        if UserDefaults.standard.string(forKey: .inAppPurchase) ==  nil{
              buttonViewContainer.isHidden = true
            if load {
                print("Cancel Action")
                load = false
            }else {
                startScanning()
            }
        }else{
            buttonViewContainer.isHidden = false
        }
      Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)
    }
    
    
    
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = true
                $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
            })
        }
    }
    
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            $0.showOverlayView         = true
            $0.rectOfInterest          = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
            
            $0.reader.stopScanningWhenCodeIsFound = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    @IBAction func scanInModalAction(_ sender: AnyObject) {
        startScanning()
    }
    
    func addScanFromUI() {
        let subv = UIView.init(frame: CGRect.init(x: readerVC.view.frame.size.width/2 - 30, y: 35, width: 60, height: 60))
        subv.backgroundColor = .red
        let glbtn = UIButton.init(frame: subv.frame)
        glbtn.setImage(UIImage.init(named: ""), for: .normal)
        //.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        glbtn.addTarget(readerVC, action: #selector(readerVC.galleryButtonAction), for: .touchUpInside)
        subv.addSubview(glbtn)
     //   readerVC.view.addSubview(subv)
    }

    
    func startScanning(){
        guard checkScanPermissions() else { return }
        
        addScanFromUI()
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                let systemSoundID: SystemSoundID = 1012
                AudioServicesPlaySystemSound (systemSoundID)
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    @IBAction func scanInPreviewAction(_ sender: Any) {
        
        showPhotoGallery()
        /*
         guard checkScanPermissions(), !reader.isRunning else { return }
         
         reader.didFindCode = { result in
         print("Completion with result: \(result.value) of type \(result.metadataType)")
         }
         
         reader.startScanning()
         
         */
    }
    
    
    // prepare(for: "showWebView", sender: self)
                  //             func prepare(for segue: UIStoryboardSegue, sender: Any?)
                  //            {
                  //                if segue.destination is WebViewController
                  //                {
                  //                    let vc = segue.destination as? WebViewController
                  //                    vc?.search = url
                  //                }
                  //            }
                  //UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        print("after result block")
        
        reader.stopScanning()
        dismiss(animated: true) { [weak self] in
            
            var str = result.value
            
           
            if UserDefaults.standard.getIsSaveScanData(forKey: .saveScanData){
                self?.saveScanInformation(result: str, searchFrom: 2, imageData: UIImage.init())
            }
            
            if result.value.canOpenURL() && UserDefaults.standard.getIsOpenURLInBrowser(forKey: .openURLInBrowser) {
            
                if let url = URL(string: str) {
                    let safariController = SFSafariViewController(url: url)
                    self?.present(safariController, animated: true, completion: nil)
                }
            }
                
            else if UserDefaults.standard.getIsOnlineSearchScanData(forKey: .onlineSearchAfterScan) {
                str = str.replacingOccurrences(of: " ", with: "+")
                let url = "https://www.google.co.in/search?q=" + str
                self?.openSearchInViewController(searchParam: url)
            }else {
                self!.load = false
                self!.readerDidCancel(reader)
                let alert = UIAlertController(
                    title: "QRCodeReader",
                    message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                    preferredStyle: .alert
                )
                
                if UserDefaults.standard.getIsSaveScanData(forKey: .saveScanData){
                    AppStoreReviewManager.requestReviewIfAppropriate()
                    self?.tabBarController?.selectedIndex = 2
                }else {
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self?.present(alert, animated: true, completion: {
                        
                    })
                }
            }
            
            
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        load = true
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        
        
    }
    
    
    //Mark: User Defined Method
    
    func saveScanInformation(result:String,searchFrom:Int,imageData:UIImage) {
        
        if searchFrom == 1 {  // scan Photo Gallery
            
            let filename = String(format: "%ld.jpg",Date().millisecondsSince1970)
            
            let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),filename)
            print(imagePath)
            let data = imageData.jpegData(compressionQuality: 1.0)//Set image quality here
            
            let fileURL = URL.init(fileURLWithPath: imagePath)
            do {
                // writes the image data to disk
                try data?.write(to: fileURL, options: .atomic)
            } catch {
                print("error:", error)
            }
            
            let codeInfo = CodeInfo.init(id: 0, codeName: result, codeType: 1, date: Date.init(), operationType: 2, codeImage:filename , isFavorite: false)
            DBHelper.inseCodeInformation(codeInfo: codeInfo)
            self.tabBarController?.selectedIndex = 2
        }else { // scan using camera
            
            let codeInfo = CodeInfo.init(id: 0, codeName: result, codeType: 1, date: Date.init(), operationType: 2, codeImage:"" , isFavorite: false)
            DBHelper.inseCodeInformation(codeInfo: codeInfo)
        }
    }
    
    func openSearchInViewController(searchParam:String){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let webView = storyboard.instantiateViewController(withIdentifier: "webViewController") as! WebViewController
        webView.search = searchParam
        navigationController?.pushViewController(webView, animated: true)
        // present(webView, animated: true, completion: nil)
    }
    
    
    func showPhotoGallery() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
}


extension ScanViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let qrcodeImg = info[.originalImage] as? UIImage {
            
            let detector:CIDetector=CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])!
            let ciImage:CIImage=CIImage(image:qrcodeImg)!
            var qrCodeLink=""
            
            let features=detector.features(in: ciImage)
            for feature in features as! [CIQRCodeFeature] {
                qrCodeLink += feature.messageString!
            }
            
            if qrCodeLink=="" {
                print("nothing")
            }else{
                saveScanInformation(result: qrCodeLink, searchFrom: 1, imageData: qrcodeImg)
            }
        
    }
    else{
    print("Something went wrong")
    }
    self.dismiss(animated: true, completion: nil)
    }
}


