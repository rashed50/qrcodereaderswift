//
//  createHistoryController.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit

class ScanHistoryController: UIViewController {
     var createHistory   = [CodeInfo]()
        @IBOutlet weak var tableView: UITableView!
        override func viewDidLoad() {
            super.viewDidLoad()
            self.tableView.register(createHistoryTableCell.nib, forCellReuseIdentifier: createHistoryTableCell.identifier)
            Helper.addBannerInViewToBottom(viewController: self, tabBarController: self.tabBarController!)

        }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "QR Code Details")
          createHistory = DBHelper.getAllCodeInformationWithOperationType(optType: 2) // 2 scan histry
        self.tableView.reloadData()
        
    }

    }
    extension ScanHistoryController: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return createHistory.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "createHistoryTableCell", for: indexPath) as! createHistoryTableCell
            
                let codeInfo : CodeInfo = createHistory[indexPath.row]
                cell.createCodeNameLabel.text = codeInfo.codeName
                let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage)
                cell.createCodeImage?.image = UIImage.init(contentsOfFile: imagePath)
                cell.createCodeDateLabel.text = Helper.getStringFromDate(date:codeInfo.createDate)
            cell.favouriteImageView.isHidden = true
            if codeInfo.isFavorite{
                cell.favouriteImageView.image = Helper.getFavouriteImage()
                cell.favouriteImageView.isHidden = false
                
            }
 
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            openActionSheetView(index: indexPath.row)
           
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70
        }
        
        
        
        
   func openActionSheetView(index:Int)
           {
               
               let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
               
               let information = UIAlertAction(title: "Preview", style: .default, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   
                   if #available(iOS 13.0, *) {
                       if let vc = self.storyboard?.instantiateViewController(identifier: "qRCodePreviewController") as? QRCodePreviewController{
                           vc.codeInfo = self.createHistory[index]
                           self.navigationController?.pushViewController(vc, animated: true)
                       }
                   } else {
                       
                   }
                   
                   
               })
               
               let codeInfo = self.createHistory[index]
               let favouriteActionText = codeInfo.isFavorite ? "Unfavourite" : "Favourite"
               let favouriteAction = UIAlertAction(title: favouriteActionText, style: .default, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   
                   codeInfo.isFavorite = !codeInfo.isFavorite
                   self.createHistory[index] = codeInfo
                   DBHelper.updateCodeInfoAsFavouriteUnFavourite(isFavourite: codeInfo.isFavorite, id: codeInfo.id)
                   self.tableView.reloadData()
                   
               })
               let shareAction = UIAlertAction(title: "Share", style: .default, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   self.shareButtonPressed(codeInfo: codeInfo)
               })
               let deleteAction = UIAlertAction(title: "Delete", style: .default, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   DBHelper.deleteCodeInfo(id: codeInfo.id)
                   self.createHistory.remove(at: index)
                   self.tableView.reloadData()
                   
                   
               })
               let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
               {
                   (alert: UIAlertAction!) -> Void in
                   
               })
               
               optionMenu.addAction(information)
               optionMenu.addAction(favouriteAction)
               optionMenu.addAction(shareAction)
               optionMenu.addAction(deleteAction)
               optionMenu.addAction(cancelAction)
               self.present(optionMenu, animated: true, completion: nil)
           }
           
           
           
        func shareButtonPressed(codeInfo : CodeInfo){
        
             
            let imagePath = String(format: "%@/%@", Helper.getQRCodeImagesDirectory(),codeInfo.codeImage)
            let image = UIImage(contentsOfFile: imagePath)
             
               if image != nil
               {
                  let imageToShare = [ image! ]
                  // let objectsToShare = [message,link] as [Any]
                   let activityVC = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                   activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                   self.present(activityVC, animated: true, completion: nil)
               }

           }
        
    }




