//
//  Helper.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import Foundation
import UIKit

let APP_HEXA_COlOR_CODE = "c10547"
let APP_TEXT_HEXA_COlOR_CODE = "ffffff"

class Helper {
    static func GetPlacheholderText(index:Int8)->String {
        if index == 0 {
            return "http://apple.com"
        }
        else if index == 1 {
            return "(880)1731540704"
        }
        else if index == 2 {
            return "Enter your text"
        }
        else if index == 3 {
            return "rashed084050@gmail.com"
        }
        else if index == 4 {
            return ""
        }
        else if index == 5 {
            return "Enter your SMS"
        }
        else if index == 7 {
            return ""
        }
        return ""
    }
    
    static func GetDetailsText(index:Int8)->String {
        if index == 0 {
            return "Enter the valid URL that you'd like to create  QR Code"
        }
        else if index == 1 {
            return "Enter the phone number that you'd like to create QR Code"
        }
        else if index == 2 {
            return "Enter the Text that you'd like to create QR Code"
        }
        else if index == 3 {
            return "Enter the Email Address that you'd like to create QR Code"
        }
        else if index == 4 {
            return "Enter the Contact Details that you'd like to create QR Code"
        }
        else if index == 5 {
            return "Enter the SMS Details that you'd like to create QR Code"
        }
        else if index == 7 {
            return "Choos your Calendar Event that you'd like to create QR Code"
        }else if index == 8{
            return "Enter the Wireless Network Details that you'd like to create QR Code"
        }
        return ""
    }
    
    //
    // MARK: DOCUMENT DIRECTORY HELPER METHOD
    //
    
    
    static func getFavouriteImage()->UIImage{
        return UIImage.init(named: "favourite.png")!
    }
    
    static  func getDocumentDirectory() -> String {
        
        if let documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            return documentsPathURL.absoluteString
        }
        return ""
    }
    static  func getQRCodeImagesDirectory() -> String {
        return self.getBasePath(basePath: "QRCodeImages")
    }
    
    private  static func getBasePath( basePath:String ) -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        let dataPath = documentsDirectory.appending("/\(basePath)")
        return dataPath
    }
    
    public  static  func createQRCodeImagesDirectory() {
        do {
            try FileManager.default.createDirectory(atPath:self.getBasePath(basePath: "QRCodeImages"), withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
            
        }
    }
    
    
    static func getPathForQrCodeLogo()-> String{
               var path =  Helper.getQRCodeImagesDirectory()
              path = String(format: "%@/%@", path,"logo.jpg")
        return path
    }
    
    
    
    //
    // MARK: Operation Helper METHOD
    //
    
    static func getStringFromDate(date:Date)-> String{
        
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy HH:mm a"
        return df.string(from: date)
    }
    
    
    
    static  func hexStringToUIColor(hexa: String) -> UIColor {

                   // Convert hex string to an integer
                            var hexInt: UInt32 = 0
                           // Create scanner
                           let scanner: Scanner = Scanner(string: hexa)
                           // Tell scanner to skip the # character
                           scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
                           // Scan hex value
                           scanner.scanHexInt32(&hexInt)
         
                   let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
       
                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
    
    
   static func hexStringFromColor(color: UIColor) -> String {
          let components = color.cgColor.components
          let r: CGFloat = components?[0] ?? 0.0
          let g: CGFloat = components?[1] ?? 0.0
          let b: CGFloat = components?[2] ?? 0.0

          let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
          print(hexString)
          return hexString
       }
    
    
    
    
    
     //
     // MARK: SET NAVIGATION BAR PROPERTY
     //
        @objc
       static func setNavigationBarProperty(navbar:UINavigationController, size: Int ,title:String){
           
           let ssize  = CGFloat(size)
           let attrs = [
               NSAttributedString.Key.foregroundColor:getTextColor(),NSAttributedString.Key.font: UIFont(name:getAppTextFontName(), size:ssize)!
            ]
           navbar.navigationBar.titleTextAttributes = attrs
           navbar.navigationBar.topItem?.title = title
           navbar.navigationBar.backgroundColor = getAppColor()
           navbar.navigationBar.barTintColor = getAppColor()
       }
       
       @objc
         static func setTabBarProperty(tabBar:UITabBarController){
           
           tabBar.tabBar.barTintColor = getAppColor()
            tabBar.tabBar.tintColor = UIColor.black
           tabBar.tabBar.unselectedItemTintColor = getTextColor()

          
         }

       
    
      private static  func getHexaStringToColor(hexa: String) -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: hexa))  //
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
       
                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
       
         @objc
         static  func getAppColor() -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: APP_HEXA_COlOR_CODE))  //
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    
                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
         @objc
         static  func getTextColor() -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: APP_TEXT_HEXA_COlOR_CODE))
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
         static func intFromHexString(hexStr: String) -> UInt32 {
                var hexInt: UInt32 = 0
                // Create scanner
                let scanner: Scanner = Scanner(string: hexStr)
                // Tell scanner to skip the # character
                scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
                // Scan hex value
                scanner.scanHexInt32(&hexInt)
                return hexInt
            }
       private static func getAppTextFontName()-> String{
           
          // return "HelveticaNeue-Bold"
            return "MarkerFelt-Wide"
         // return  "Georgia-Bold"
       }
       
       @objc static func getAppName()-> String{
           
         
            return "QR Kit Pro"
         
       }
         
       @objc  static func SetButtonUIProperty(button: UIButton,title:String,fontSize:Float){
             
             let fontSize = CGFloat(fontSize)
             button.backgroundColor = getAppColor()//  C80000
             if(title != ""){
                 button.setTitle(title, for: .normal)
             }
             button.setTitleColor(getTextColor(), for: .normal)
            button.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
            button.layer.cornerRadius = 10
              
         }
    @objc  static func SetBarButtonUIProperty(button: UIBarButtonItem,fontSize:Float){
          
          let fontSize = CGFloat(fontSize)
          button.tintColor = getAppColor()//  C80000
        button.setTitleTextAttributes([NSAttributedString.Key.foregroundColor :self.getTextColor(),NSAttributedString.Key.font:UIFont.init(name:getAppTextFontName(), size:fontSize)!], for: UIControl.State.normal)

      }
    
    
    //
       // MARK: GOOGLE ADSMOB
       //
       
    static func addBannerInViewToBottom(viewController: UIViewController,tabBarController: UITabBarController) {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.adMobBannerView.rootViewController = viewController
        
               let bottomMargin = tabBarController.tabBar.frame.size.height  // as Float
               appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                                                          y: viewController.view.frame.height - bottomMargin - appDelegate.adMobBannerView.frame.height,
                                                          width: viewController.view.frame.width,
                                        height: appDelegate.adMobBannerView.frame.height)
              appDelegate.adMobBannerView.isHidden = false
        if UserDefaults.standard.string(forKey: .inAppPurchase) == nil{
           // viewController.view.addSubview(appDelegate.adMobBannerView)
            
        }

       
       
    
}

}

