//
//  Information.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 07/08/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import Foundation
import UIKit
class Information {
    static func codeActionSheet()
       {
           let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)

           let information = UIAlertAction(title: "Code Information", style: .default, handler:
           {
               (alert: UIAlertAction!) -> Void in
             
           })

           let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
           {
               (alert: UIAlertAction!) -> Void in
             
           })
          
           optionMenu.addAction(information)
           optionMenu.addAction(cancelAction)
           self.presentViewController(optionMenu, animated: true, completion: nil)
       }
       
}
