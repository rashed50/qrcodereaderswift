//
//  DBHelper.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 30/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import Foundation
import UIKit
import GRDB


class DBHelper: NSObject {
    
    static let dbPath = "\(Helper.getDocumentDirectory())/\("qrcode.sqlite")"
    static var dbQueue = DatabaseQueue()
    

    
    
    static func CreateDatabase(){
        do
        {
            
            dbQueue = try DatabaseQueue(path: dbPath)
            // dbQueue.path
            try dbQueue.inDatabase
            {
                db in try db.execute(sql: "CREATE TABLE CODEINFO ( id INTEGER PRIMARY KEY AUTOINCREMENT,codeName TEXT ,codeType INT , createDate DATE, operationType INT ,codeImage TEXT ,isFavorite BOOL)")
 
                
            }
        }
        catch (let error)
        {
            print("Erreur !\(error)")
        }
    }
    
    
    static func inseCodeInformation(codeInfo : CodeInfo){
        do {
            
            try dbQueue.write { db in
                
                try db.execute(
                    sql: "INSERT INTO CODEINFO (codeName,codeType,createDate,operationType,codeImage,isFavorite) VALUES (?,?,?,?,?,?)",
                    arguments: [codeInfo.codeName,codeInfo.codeType,codeInfo.createDate,codeInfo.operationType,codeInfo.codeImage,codeInfo.isFavorite])
              
                //   try db.execute(
                //  sql: "UPDATE player SET score = :score WHERE id = :id",
                //  arguments: ["score": 1000, "id": 1])
                // }
            }
            
        } catch (let errorr) {
            print(errorr)
        }
        
    }
    
    // Read Data from CodeInfo Database
    
    static func getAllCodeInfoInformation()-> [CodeInfo]{
           var codeInfoList = [CodeInfo]()
           do{
               try dbQueue.read { db in
                   let allrow = try Row.fetchAll(db, sql: "SELECT * FROM CODEINFO ORDER BY id DESC")
                       for row in allrow{
                           let codeId :Int64 = row["id"]
                           let codeName: String = row["codeName"]
                           let codeType: Int8 = row["codeType"]
                           let codeDate: Date = row["createDate"]
                           let codeOperation: Int8 = row["operationType"]
                           let codeImage : String = row["codeImage"]
                           let codeFavourite: Bool = row["isFavorite"]
                         

                        
                        let  code :CodeInfo =  CodeInfo.init(id: codeId, codeName: codeName, codeType: codeType, date: codeDate, operationType: codeOperation, codeImage: codeImage, isFavorite: codeFavourite)
                           codeInfoList.append(code);
                           
                           
                       }
               
               }
           }catch (let errorr) {
               print(errorr)
           }
           
           return codeInfoList
       }

    
    // optType = 1 for create code and 2 for scan code
    static func getAllCodeInformationWithOperationType(optType :Int)-> [CodeInfo]{
         
        
              var codeInfoList = [CodeInfo]()
              do{
                  try dbQueue.read { db in
                      let allrow = try Row.fetchAll(db, sql: "SELECT * FROM CODEINFO WHERE  operationType = ?  ORDER BY id DESC",arguments: [optType])
                          for row in allrow{
                              let codeId :Int64 = row["id"]
                              let codeName: String = row["codeName"]
                              let codeType: Int8 = row["codeType"]
                              let codeDate: Date = row["createDate"]
                              let codeOperation: Int8 = row["operationType"]
                              let codeImage : String = row["codeImage"]
                              let codeFavourite: Bool = row["isFavorite"]
               
                            
                           
                           let  code :CodeInfo =  CodeInfo.init(id: codeId, codeName: codeName, codeType: codeType, date: codeDate, operationType: codeOperation, codeImage: codeImage, isFavorite: codeFavourite)
                              codeInfoList.append(code);
                              
                              
                          }
                  
                  }
              }catch (let errorr) {
                  print(errorr)
              }
              
              return codeInfoList
          }
    
    
    
    static func updateCodeInfoAsFavouriteUnFavourite(isFavourite:Bool, id : Int64){
        do {
            
            try dbQueue.write { db in
                  try db.execute(
                  sql: "UPDATE CODEINFO SET isFavorite = ? WHERE id = ? ",
                  arguments: [isFavourite ,id])
                 
            }
            
        } catch (let errorr) {
            print(errorr)
        }
    }

    
    static func deleteCodeInfo(id : Int64){
        do {
            
            try dbQueue.write { db in
                  try db.execute(
                  sql: "DELETE FROM CODEINFO WHERE id = ?",
                  arguments:[id])
            }
            
        } catch (let errorr) {
            print(errorr)
        }
    }
    
}
