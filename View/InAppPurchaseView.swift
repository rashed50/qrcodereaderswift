//
//  InAppPurchaseView.swift
//  QRCodeReader.swift
//
//  Created by Rashedul Hoque on 14/8/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import UIKit
import StoreKit
import SVProgressHUD

class InAppPurchaseView: UIView {
    let kCONTENT_XIB_NAME = "InAppPurchaseView"
    @IBOutlet var contentView: InAppPurchaseView!
    @IBOutlet var appTittleLabel: UILabel!
    @IBOutlet var appPriceLabel: UILabel!
    
    @IBOutlet weak var restoreButtonOutlet: UIButton!
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBOutlet weak var purchaseButtonOutlet: UIButton!

    @IBOutlet weak var noAdsButton: UIButton!
    @IBOutlet weak var noAdsDescriptionLabel: UILabel!
    @IBOutlet weak var customQRCodeButton: UIButton!
    @IBOutlet weak var customQRCodeLabel: UILabel!
    @IBOutlet weak var scanFromGalleryButton: UIButton!
    @IBOutlet weak var scanFromGalleryLabel: UILabel!
    @IBOutlet weak var fingerLockButton: UIButton!
    @IBOutlet weak var fingerLockLabel: UILabel!
    
  static  var view1 =  InAppPurchaseView().loadNib() as! InAppPurchaseView
    
    
    // Create the Activity Indicator
  static  let activityIndicator = UIActivityIndicatorView(style: .gray)

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    static  func setViewInformation(productTitle:String,price: String ,frame: CGRect)->InAppPurchaseView {
        // let view = InAppPurchaseView().loadNib() as! InAppPurchaseView
        view1.frame = frame
        view1.translatesAutoresizingMaskIntoConstraints = true
     //   view.appTittleLabel.text = productTitle
        view1.appPriceLabel.text = price
        
                
        // Add it to the view where you want it to appear
       // view1.addSubview(activityIndicator)
        activityIndicator.style = .gray
        activityIndicator.color = .red
        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        }
        
                
        // Set up its size (the super view bounds usually)
        activityIndicator.frame = CGRect.init(x: 200, y: 300, width: 100, height: 100)
        // Start the loading animation
        activityIndicator.startAnimating()
        
       
        
         
        return view1
    }
    
  static  func inAppPurchaseDone() {
 
    SVProgressHUD.dismiss()
    view1.removeFromSuperview()
    }
    
    static  func dismissProgressHud() {
     
       SVProgressHUD.dismiss()
       
       }
   
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    
    @IBAction func restoreButtonAction(_ sender: UIButton) {
        SVProgressHUD.show()
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    @IBAction func purchaseButtonAction(_ sender: Any) {
       let appDele = UIApplication.shared.delegate as! AppDelegate
        if(appDele.inAppPurchaseProducts.count > 0){
        SVProgressHUD.show()
        RazeFaceProducts.store.buyProduct(appDele.inAppPurchaseProducts[0])
        }else {
        
        }
    }
    
}




