//
//  StringExtension.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 25/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import Foundation
import UIKit

    extension String {

        
        func isValidEmail() -> Bool {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

            let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: self)
        }
        func isValidURL() -> Bool {
            guard !contains("..") else { return false }

            let head     = "((http|https)://)?([(w|W)]{3}+\\.)?"
            let tail     = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
            let urlRegEx = head+"+(.)+"+tail

            let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
            return urlTest.evaluate(with: trimmingCharacters(in: .whitespaces))
        }
        
        
        func canOpenURL() -> Bool {
           

            
                let regEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
                let predicate = NSPredicate(format: "SELF MATCHES %@", argumentArray: [regEx])
                return predicate.evaluate(with: self)
            
        }
        
      //  var data = message.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)

        
        /// Creates a QR code for the current URL in the given color.
         func qrImage(using color: UIColor) -> CIImage? {
             return qrImage?.tinted(using: color)
         }

         /// Returns a black and white QR code for this URL.
         var qrImage: CIImage? {
             guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
            let qrData = self.data(using: String.Encoding.ascii)
             qrFilter.setValue(qrData, forKey: "inputMessage")

             let qrTransform = CGAffineTransform(scaleX: 12, y: 12)
             return qrFilter.outputImage?.transformed(by: qrTransform)
         }
         
         
         func qrImage(using color: UIColor, logo: UIImage? = nil) -> CIImage? {
             let tintedQRImage = qrImage?.tinted(using: color)

             guard let logo = logo?.cgImage else {
                 return tintedQRImage
             }

             return tintedQRImage?.combined(with: CIImage(cgImage: logo))
         }
        

    }




