/// Copyright (c) 2018 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation
import UIKit

extension UserDefaults {
  enum Key: String {
    case appLock
    case reviewWorthyActionCount
    case lastReviewRequestAppVersion
    case saveScanData
    case customLogoWithQRCode
    case onlineSearchAfterScan
    case openURLInBrowser
    case qrcodeColor
    case inAppPurchase
    case firstTimeInitialize
    
  }

  func integer(forKey key: Key) -> Int {
    return integer(forKey: key.rawValue)
  }

  func string(forKey key: Key) -> String? {
    return string(forKey: key.rawValue)
  }
    func bool(forKey key:Key) -> Bool {
         return bool(forKey: key.rawValue)
    }

  func set(_ integer: Int, forKey key: Key) {
    set(integer, forKey: key.rawValue)
       UserDefaults.standard.synchronize()
  }

  func set(_ object: Any?, forKey key: Key) {
    set(object, forKey: key.rawValue)
    UserDefaults.standard.synchronize()
  }
    func set(_ bol: Bool, forKey key: Key) {
      set(bol, forKey: key.rawValue)
      UserDefaults.standard.synchronize()
    }

    func setAppLock(_ bol: Bool, forKey key: Key) {
        set(bol, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
     
    func getAppLock(forKey key: Key) -> Bool {
        print(bool(forKey: key.rawValue))
        return bool(forKey: key.rawValue)
    }
    
    
    func setIsSaveScanData(_ bol: Bool, forKey key: Key) {
        set(bol, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
     
    func getIsSaveScanData(forKey key: Key) -> Bool {
        print(bool(forKey: key.rawValue))
        return bool(forKey: key.rawValue)
    }
    
    func setCustomLogoWithQRCode(_ bol: Bool, forKey key: Key) {
           set(bol, forKey: key.rawValue)
           UserDefaults.standard.synchronize()
       }
        
       func getIsAddCustomLogoWithQRCode(forKey key: Key) -> Bool {
           print(bool(forKey: key.rawValue))
           return bool(forKey: key.rawValue)
       }
    
    
    func setOnlineSearchScanData(_ bol: Bool, forKey key: Key) {
           set(bol, forKey: key.rawValue)
           UserDefaults.standard.synchronize()
       }
        
       func getIsOnlineSearchScanData(forKey key: Key) -> Bool {
           print(bool(forKey: key.rawValue))
           return bool(forKey: key.rawValue)
       }
    
    
    func setOpenURLInBrowser(_ bol: Bool, forKey key: Key) {
           set(bol, forKey: key.rawValue)
           UserDefaults.standard.synchronize()
       }
        
       func getIsOpenURLInBrowser(forKey key: Key) -> Bool {
           print(bool(forKey: key.rawValue))
           return bool(forKey: key.rawValue)
       }
    func setQRCodeColor(hexaString:String,forKey key:Key) {
        set(hexaString, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
     
    func getQRCodeColor(forKey key:Key) -> UIColor {

        print(string(forKey: key.rawValue) as Any)
        let str = string(forKey: key.rawValue)
        return Helper.hexStringToUIColor(hexa: str ?? "0000000")
       
    }
}
