//
//  DVLocation.swift
//  PEEMZ
//
//  Created by Moin Uddin on 12/7/16.
//  Copyright © 2016 David Ghouzi. All rights reserved.
//

import UIKit

extension Date{
    var millisecondsSince1970:Int64 {
        return Int64(self.timeIntervalSince1970)*1000
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds))
    }
    
  
    
    
}
