//
//  CodeInfo.swift
//  QRCodeReader.swift
//
//  Created by JOHN DARK on 30/07/20.
//  Copyright © 2020 Yannick Loriot. All rights reserved.
//

import Foundation

class CodeInfo {
    
   // var instance = CodeInfo(id: <#T##Int64#>, codeName: <#T##String#>, codeType: <#T##Int8#>, date: <#T##Date#>, operationType: <#T##Int8#>, codeImage: <#T##String#>, isFavorite: <#T##Bool#>)
    
    var id:Int64
    var codeName:String
    var codeType:Int8
    var createDate:Date
    var operationType:Int8
    var codeImage:String
    var isFavorite:Bool
    
    init() {
        id = 0
        codeName = ""
        codeType = 0
        createDate = Date()
        operationType = 1
        codeImage = ""
        isFavorite = false
    }
     init(id:Int64, codeName:String, codeType:Int8, date:Date, operationType:Int8, codeImage:String, isFavorite:Bool) {
        
        self.id = id
        self.codeName = codeName
        self.codeType = codeType
        self.createDate = date
        self.operationType = operationType
        self.codeImage = codeImage
        self.isFavorite = isFavorite
    }
    
}
